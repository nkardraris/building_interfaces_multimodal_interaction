#!/usr/bin/python
import sys

dictionary = sys.argv[1]
pronunciations = sys.stdout
word_list = sys.stdin

words = []
for ln in word_list:
    ln = ln.rstrip('\r\n')
    words.append(ln)

dico = open(dictionary, 'r')
words_with_pron = []
for ln in dico:
    ln = ln.rstrip('\r\n')
    ln_info = ln.split(' ')
    word = ln_info[0]
    if (word in words):
	words_with_pron.append(word)
        print >> pronunciations, ln

for word in words:
    if (word not in words_with_pron):
      print >> pronunciations, word

dico.close()
