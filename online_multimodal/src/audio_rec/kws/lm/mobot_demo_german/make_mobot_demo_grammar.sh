output_name='mobot_demo_commands'
mobot_cmd_list='mobot_commands.txt.selection'

# extract word sequences from voxforge transcriptions
python find_prompts_with_pron.py  german_voxforge_prompts ../../dict/german_voxforge.dict 50 3

# select 1-grams 2-grams and 3-grams
head -n 5 german_voxforge_prompts.selection | awk '{print $1}' > german_voxforge_prompts.selection.tmp 
head -n 30  german_voxforge_prompts.selection | tail -n 15 | awk '{print $1,$2}' >> german_voxforge_prompts.selection.tmp 
head -n 45  german_voxforge_prompts.selection | tail -n 15 | awk '{print $1,$2,$3}' >> german_voxforge_prompts.selection.tmp 
mv german_voxforge_prompts.selection.tmp german_voxforge_prompts.selection

# create grammar word net
cat ${mobot_cmd_list} german_voxforge_prompts.selection | sed -r 's/\s+/ /g;s/\s+$//g' > mobot_demo_commands.utts
python ../scripts/utts2gram.py < mobot_demo_commands.utts > mobot_demo_commands.gram
HParse mobot_demo_commands.gram ${output_name}.wdnet

# create dictionary
cat ../../dict/german_voxforge.dict.selection ../../dict/mobot_cmds_words.dict | awk '{printf("%s sp\n",$0)}' \
	| sort -u  | sed -r 's/\s+/ /g' > ../../dict/${output_name}.dict
