#!/usr/bin/python
import sys
import re

prompts_in = sys.argv[1]
dict_in = sys.argv[2]
nUtt = int(sys.argv[3])
nWords = int(sys.argv[4])

D = {}
# read dict
for pron in open(dict_in):
	pron = pron.rstrip('\r\n')
	F = re.split('\s+',pron)
	D[F[0]] = F[1:len(F)-1]

# filter prompts
c1 = 0
f1 = open('%s.selection' % (prompts_in),'w')
f2 = open('%s.selection' % (dict_in),'w')
for prompt in open(prompts_in):
	c1 = c1 + 1
	prompt = prompt.rstrip('\r\n')
	missing_prons = False
	words = []
	dict_entries = []
	for word in re.split('\s+',prompt)[1:]:
	  if word and word in D.keys():
	    words.append(word)
	    dict_entries.append('%s %s' % (word," ".join(D[word])))	
	  if len(words) >= nWords:
	     f1.write(" ".join(words) + '\n')
	     f2.write('\n'.join(dict_entries))
	     break
	f2.write('\n')
	if c1 >= nUtt:
	   break 
