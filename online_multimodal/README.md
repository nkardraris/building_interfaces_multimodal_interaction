**This repo is still under construction and will be finalized soon.  
If you're looking for the software submitted at the ACMMM'16 OSSC, please use the link provided at the [project page](http://robotics.ntua.gr/software/building-interfaces-multimodal-interaction/).**

### Installation

#### Hardware Dependencies:
* Linux PC (we have tested under Ubuntu 12.04 and 14.04)
* A compatible camera (we use "Kinect for Windows" )
* A compatible microphone (we use a 8-channel MEMS microphone array)

#### Software dependencies:
* Robot Operating System (ROS); we have tested groovy and hydro
* OpenCV
* Python with the SciPy module
* vlfeat package
* libsvm package

#### Install dependencies
######1. ROS
You can install ROS either from your [Linux distribution's packages](http://wiki.ros.org/indigo/Installation/Ubuntu) (recommended) or [from source](http://wiki.ros.org/groovy/Installation/Source#Installing_on_Ubuntu_from_source). We also  recommend that you choose the "Desktop-Full Install" configuration. When ROS is installed, create and initalise a catkin workspace, (e.g. in ~/catkin_ws):  
$ mkdir ~/catkin_wc  
$ mkdir ~/catkin_wc/src/  
$ cd ~/catkin_wc/src  
$ catkin_init_workspace  
$ cd ~/catkin_ws
$ catkin_make
$ source ~/catkin_ws/devel/setup.bash
$ echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc # optional; you should source this file every time you use your catkin workspace

###### 2. OpenCV
If you chose the "Desktop-Full Install" option in the above step, all necessary OpenCV packages should be already installed, so you can skip this step. Otherwise, for Ubuntu you can just install from apt:
$ sudo apt-get install libopencv-dev python-opencv  
or follow the instructions  [here](http://docs.opencv.org/2.4/doc/tutorials/introduction/linux_install/linux_install.html) or [here](https://help.ubuntu.com/community/OpenCV) to build from source. We use version 2.4.2, but any 2.x version shall work.

######3. Vlfeat
Download and build vlfeat (e.g. in ~/external_pkg) as follows:
$ mkdir ~/external_pkgs
$ cd ~/external_pkgs
$ git clone https://github.com/vlfeat/vlfeat.git
$ cd vlfeat
$ make
###### 4. LibSVM
Download and build libsvm (e.g. in ~/external_pkg):
$ cd ~/external_pkg
$ git clone https://github.com/cjlin1/libsvm
$ cd libsvm
$ make
$ make lib

###### 5. HTK
Download HTK 3.4.1 from the [official website](http://htk.eng.cam.ac.uk/download.shtml) and folow the instructions in the  README file.

###### 6. alsaaudio and sox
For Ubuntu:
$ sudo apt-get install python-alsaaudio
$ sudo apt-get install sox

#### Install the online recognition system
1. Copy the ROS package into your catkin workspace, e.g.:  
$ cp -r building_interfaces_multimodal_interaction/online_multimodal ~/catkin_ws/src  
2. Adjust the paths in online_multimodal/CMakeLists.txt (lines 99-102) to point to the locations where you have downloaded and built vlfeat and libsvm header files and libraries (see the file provided for an example).
3. Build:  
$ cd ~/catkin_ws  
$ catkin_make  
4. Download pre-trained audio/visual models ([sample](???)) or use your own. 
5. Adjust the parameters in the following config files to match your setup. Use the path where you' ve stored your pre-traind models and the HTK executables:  
online_multimodal/config/action_dense_track.yaml (lines 8-11)  
online_multimodal/config/scr_mobot.yaml (lines 9, 24-30)  
online_multimodal/config/alwaysOnCommandReco.yaml (line 20)  
online_multimodal/config/fusion_node.yaml (for multimodal recognition, line 2)  
online_multimodal/config/fusion_node_single_modality.yaml (for audio-only or visual-only recognition, line 2)  
6. https://drive.google.com/open?id=0B5AMU5hRhkD9WmV0OGpsYnNMUUk

### Use the system
First of all, you should launch ROS master:
$ roscore
###### Audio command recognition:
$ roslaunch online_multimodal audio_command_rec.launch
###### Gesture recognition:
$ roslaunch online_multimodal gesture_rec.launch  	
After launching activity_detection.py a new window will pop up showing the camera input. 
A calibration step is required at the beginning: you will see a ~30 sec countdown. During this interval you should perform 1-2 sample gestures and stay still until the counter finishes.
Note: Make sure that the subject is approximately at the middle of the video frame, 1.5-3 meters away from the camera and their arms and hands are always visible. There should not be any background visual noise.
###### Multimodal recognition:
$ roslaunch online_multimodal multimodal_rec.launch
The same callibration step mentiond above applies here too.



