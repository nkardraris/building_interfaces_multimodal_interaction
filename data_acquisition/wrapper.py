import os, sys
import subprocess
import time
import shlex
import code
import datetime

def main(procs):
    # kinect
    commands =  ['roscore',
                'roslaunch openni_launch openni.launch depth_registration:=true', 
                'rosrun image_view image_view image:=/camera/rgb/image_color',
                'rosrun image_view image_view image:=/camera/depth_registered/image_rect',#/camera/depth/image
                'python audiopublisher.py',
                'python app.py']

    # rosbags
    #commands =  ['roscore',
                #'rosbag play --rate=0.8 /mobot_data/Mobot_Heidelberg/p1_2/6.a/2013-11-24-11-56-35.bag', 
                #'rosrun image_view image_view image:=/camera1/rgb/image_color/',
                #'rosrun image_view image_view image:=/camera1/depth_registered/image_rect/',
                #'python app.py']
    
    FNULL = open(os.devnull, 'w')
    for c in xrange(len(commands)):
        args = shlex.split(commands[c])
        print args
        try:
            procs.append(subprocess.Popen(args))
            #procs.append(subprocess.Popen(args, stdout=logfile, stderr=logfile))
            time.sleep(2)
        except:
            print('Error excecuting %s.' % commands[c])
    
    time.sleep(3600)
    raise KeyboardInterrupt
    check_procs(procs)
    #code.interact(local=locals())
    

def check_procs(procs):
    print('Terminating subprocesses...\n')
    for p in procs:
        try:
            p.terminate()
        except:
            print('Error terminating %d.' % p.pid)
    time.sleep(5)
    for p in procs:
        if p.poll() is None:
            print "killing process %d" % p.pid
            try:
                p.kill()
            except:
                print('Error killing %d.' % p.pid )               
    

if __name__ == '__main__':
    try:
        #logfile_name = os.path.abspath("stdout_%s_%d" %(datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S"), os.getpid()))
        #logfile = open(logfile_name, "w")
        p = []
        main(p)
    except:
        check_procs(p)
        #logfile.close()
        exit()
