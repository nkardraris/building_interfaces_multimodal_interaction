#!/usr/bin/env python
# -*- coding: utf-8 -*-
from functools import partial
import itertools
import mimetypes
import os
import re
import time
import unicodedata
import warnings

import remi.gui as gui
from remi import start, App
import yaml

from srtgen import SrtGen
import rosrecoder
from pprint import pprint

"""
if it hangs: socket.error: [Errno 98] Address already in use
ps -aef|grep [r]emi|tr -s " " |cut -d' ' -f2 |xargs -n1 kill -9
"""

#ROS_TOPICS = ["/camera/rgb/image_color", "/camera/depth_registered/image_rect", "audiorecord"]
#ROS_TOPICS = ["/camera/rgb/image_color", "/camera/depth_registered/image_rect", "audiorecord", "camera/depth_registered/camera_info", "camera/rgb/camera_info"]
ROS_TOPICS = ["/camera/rgb/image_color", "/camera/depth_registered/image_rect", "audiorecord", "camera/depth_registered/camera_info"]

# yaml key that has the list of experiments
EXPERIMENT_KEY = "Experiment"

#list box string for empty selections
DEFAULT_LB_STRING = "Select..."

#description label font size '150%'
LBL_FONT_SIZE = '200%'

#save directory with slash at the end. it should be already available and writable on the disk
OUTPUT_DIR = "/mobot_evaluation_data/databases/cvsp/acquisition/"


class FilesEnabledApp(App):
    def process_all(self, function):
        print function
        if function.startswith('/') and function.endswith('.mp4'):
            filename = os.curdir + function
            if not os.path.exists(filename):
                self.send_response(404)
                return

            mimetype,encoding = mimetypes.guess_type(filename)
            self.send_response(200)
            self.send_header('Content-type', mimetype if mimetype else 'application/octet-stream')
            if self.server.enable_file_cache:
                self.send_header('Cache-Control', 'public, max-age=86400')
            self.end_headers()
            with open(filename, 'rb') as f:
                content = f.read()
                self.wfile.write(content)
        else:
            lines = super(FilesEnabledApp, self).process_all(function)

def slugify(value):
    return value
    """
    Converts to lowercase, removes non-word characters (alphanumerics and
    underscores) and converts spaces to hyphens. Also strips leading and
    trailing whitespace.
    """
    if isinstance(value, unicode):
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    return re.sub('[-\s]+', '-', value)


class MyApp(FilesEnabledApp):

    def __init__(self, *args):
        self.experiment_current = 0
        super(MyApp, self).__init__(*args)
        #self.settings = {param:None for param in self.params }

    def DropDownGenerator(self, title, itemlist, parent_el):
        stitle = slugify(title)
        title_lbl = gui.Label(200, 10, title)
        itemlist.insert(0, DEFAULT_LB_STRING)
        parent_el.append('t_' + stitle, title_lbl)
        dropdown = gui.DropDown(200, 25)
        parent_el.append('dd_' + stitle, dropdown)
        callback_func_name = 'dd_' + stitle
        setattr(self, callback_func_name, partial(self.on_dropdown_change, key=title))
        dropdown.set_on_change_listener(self, callback_func_name)
        for i, item in enumerate(itemlist):
            listitem = gui.DropDownItem(200, 20, item)
            dropdown.append(str(i), listitem)
            #parent_el.append(str(item), item)
        return parent_el

    def main(self, name='world'):
        # the arguments are    wid,ht,layoutOrientationOrizontal,margin
        self.wid = gui.Widget(550, 1100, False, 10)
        self.experiment_current = 0

        # To make it prettier put a tittle of what is this demo
        self.title_lbl = gui.Label(350, 20, "Experiment runnner")
        self.description_lbl = gui.Label(350, 30,
            """Please select all appropriate settings.
               Thank you!""")

        # appending a widget to another, the first argument is a string key
        self.wid.append('1', self.title_lbl)
        self.wid.append('2', self.description_lbl)

        with open("setup.yaml", 'r') as stream:
            self.params = yaml.load(stream)
            self.settings = {v:None for v in self.params}

        with open("parameters.yaml", 'r') as stream:
            self.experiments = yaml.load(stream)
            pprint(self.experiments)
            self.nVideoPlayers = max(len(self.experiments[k]) for k in self.experiments)

        with open("translations.yaml", 'r') as stream:
            self.translations = yaml.load(stream)
            pprint(self.translations)

        for param in self.params:
            self.wid = self.DropDownGenerator(param, [v for v in self.params[param]], self.wid)

        self.generate_video_list()

        self.Container = gui.Widget(400, 140, True, 10)
        self.wid.append('3', self.Container)

        self.Container1 = gui.Widget(120, 120, True, 10)
        self.Container.append('1', self.Container1)
        bt1 = lambda x: gui.Button(x, x, "Record")
        bt2 = lambda x: gui.Button(x, x, "Stop")
        self.record_button = itertools.cycle([bt1, bt2]).next
        self.recording_state = 'Init'
        self.record_button_switcher()

        self.Container2 = gui.Widget(240, 140, True, 10)
        self.Container.append('2', self.Container2) # activate only during record
        bt1b = lambda x: gui.Button(x, x, "Annotate")
        bt2b = lambda x: gui.Button(x, x, "End Annotation")
        self.record_button2 = itertools.cycle([bt1b, bt2b]).next
        self.recording_state = 'Init'
        self.annotation_button_switcher()

        self.Container3 = gui.Widget(350, 160, False, 10)

        self.update_lbl_current_gesture("Select Experiment: ")

        self.Container.append("3", self.Container3)

        self.Container_vid = gui.Widget(400, 250, True, 10)
        self.wid.append("container_vid", self.Container_vid)

        return self.wid

    def update_lbl_current_gesture(self, txt):

        # progress
        try:
            i = 0 or self.experiment_current
            n = 0 or len(self.experiments[self.selected_set]) - 1
            txt = str(-~i) + "/" + str(-~n) + " - " + self._(txt)
        except AttributeError:
            i = 0
            n = 0

        # progress_bar
        self.prograss_bar = gui.Tag()
        self.prograss_bar.type = 'progress'
        self.prograss_bar.attributes['value'] = str(i)
        self.prograss_bar.attributes['max'] = str(n)
        self.prograss_bar.attributes['width'] = '350px'
        self.Container3.append("progress_bar", self.prograss_bar)

        #update label
        txt = self._(txt)
        self.lbl_current_gesture = gui.Label(350, 50, txt)
        self.lbl_current_gesture.style['font-size'] = LBL_FONT_SIZE
        self.Container3.append("lbl_current_gesture", self.lbl_current_gesture)

    def record_button_switcher(self):
        dynamic_widget = self.record_button()(100)

        if self.recording_state == 'Init':
            print "initialize recording"
            self.srt = SrtGen()
        self.recording_state = dynamic_widget.children['text']
        if self.recording_state == 'Record':
            print 'stopped recording'

            self.srt.stop_recording()
            self.experiment_current = 0

            print "stopping ros"
            try:
                if hasattr(self, 'p'):
                    print "stopped ros"
                    rosrecoder.stop(self.p)
            except Exception as ex:
               template = "An exception of type {0} occured. Arguments:\n{1!r}"
               message = template.format(type(ex).__name__, ex.args)
               print message

        elif self.recording_state == 'Stop':
            print 'recording'

            print ' generate dir/file'
            dir_name = time.strftime('%Y-%m-%d')
            if not os.path.exists(OUTPUT_DIR + dir_name):
                os.makedirs(OUTPUT_DIR + dir_name)

            file_name = time.strftime('%Y-%m-%d-%H-%M-%S')
            file_name = file_name + "-" + self.settings['Subject'] + "-" + self.selected_set
            self.filename = OUTPUT_DIR + dir_name + os.sep + file_name

            # dry run mode
            if "dry_run" in self.filename:
                srt_name = "/dev/null"
                experiment_log_name = "/dev/null"
            else:
                srt_name = self.filename + ".srt"
                experiment_log_name = self.filename + '.csv'

            # TODO SANITIZE FILENAME
            self.srt.start_recording(srt_name)

            #add csv experiment log
            print experiment_log_name
            fieldnames = self.settings.keys()
            fieldvalues = self.settings.values()

            if not os.path.isfile(experiment_log_name):
                add_header = True
                print " new log"

            fd = open(experiment_log_name, 'a', 0)
            print fd
            if add_header:
                fd.write(','.join(fieldnames) + '\n')
            fd.write(','.join(fieldvalues) + '\n')
            fd.close()


            if "dry_run" in self.filename:
                self.filename = '/dev/null'
                print "dry_run mode: NOTHING is recorded"

            self.p = rosrecoder.start(self.filename, ".", ROS_TOPICS)
            curr = self.experiments[self.selected_set][self.experiment_current]

            print " gesture " + curr
            self.update_lbl_current_gesture(curr)

            self.video = gui.VideoPlayer(400, 350, curr + '.mp4',
                'http://www.oneparallel.com/wp-content/uploads/2011/01/placeholder.jpg')
            self.video.attributes['autoplay'] = 'autoplay'
            self.Container_vid.append("next_video", self.video)

        dynamic_widget.set_on_click_listener(self, 'record_button_switcher')
        self.Container1.append('1', dynamic_widget)

    def annotation_button_switcher(self):
        if self.recording_state == 'Record':
                    print 'Cannot annotate while not recording'
                    return

        dynamic_widget = self.record_button2()(100)
        self.recording_state = dynamic_widget.children['text']

        dynamic_widget.set_on_click_listener(self, 'annotation_button_switcher')

        curr_ges = str(self.experiment_current)

        if self.recording_state == 'Annotate':
            print 'stopped annotation'
            #('/res/bg.png');
            dynamic_widget.style['background-image'] = '/res/bg.png'
            dynamic_widget.set_on_click_listener(self, 'annotation_button_switcher')
            self.Container2.append('1', dynamic_widget)

            self.bt3b = gui.Label(1, 1, "")
            self.Container2.append('2', self.bt3b)
            self.srt.end_annotation()
            # we have not started recording
            if hasattr(self, 'selected_set'):
                print curr_ges, self.experiment_current, len(self.experiments[self.selected_set])

                if self.experiment_current >= len(self.experiments[self.selected_set]):
                    print "finished gestures"
                    #self.annotation_button_switcher()
                    self.record_button_switcher()
                    self.update_lbl_current_gesture("Recording Finished")
                    self.experiment_current = 0
                    return

                curr = self.experiments[self.selected_set][self.experiment_current]
                print "gesture from annotate " + curr
                self.update_lbl_current_gesture(curr)

                self.video = gui.VideoPlayer(400, 350, curr + '.mp4',
                    'http://www.oneparallel.com/wp-content/uploads/2011/01/placeholder.jpg')
                self.video.attributes['autoplay'] = 'autoplay'
                self.Container_vid.append("next_video", self.video)

        if self.recording_state == 'End Annotation':
            print 'annotating'

            #colorize the button
            dynamic_widget.style['background-image'] = 'none'
            dynamic_widget.style['background-color'] = 'DarkRed'
            dynamic_widget.set_on_click_listener(self, 'annotation_button_switcher')
            self.Container2.append('1', dynamic_widget)

            curr = self.experiments[self.selected_set][self.experiment_current]
            self.experiment_current += 1
            self.srt.annotate(curr)

            self.bt3b = gui.Button(100, 100, "Wrong Annotation")
            self.bt3b.set_on_click_listener(self, 'on_wrong_annotation_clicked')
            self.Container2.append('2', self.bt3b)

    def on_wrong_annotation_clicked(self):
        print "wrong annotation"
        self.experiment_current -= 1
        self.srt.wrong_annotation()
        # TODO CHECK IF LABEL BACKTRACKED
        self.annotation_button_switcher()

    def on_dropdown_change(self, value, key):

        print value, key
        self.settings[key] = value
        pprint(self.settings)

        if key == EXPERIMENT_KEY:
            if value == DEFAULT_LB_STRING:
                value = None
            self.generate_video_list(self.experiments[value])
            self.selected_set = value
            #print self.experiments[self.selected_set][self.experiment_current]

    def generate_video_list(self, video_list=None):
        return
        # we dont want video list

        self.widget_placeholder = []
        for i in range(2*self.nVideoPlayers):
            self.widget_placeholder.append(gui.Label(350, 1, ""))
            self.wid.append('placeholder_'+str(i), self.widget_placeholder[i])

        if not video_list:
            return

        pprint(video_list)
        video_list = list(set(video_list))
        self.video_widgets = []
        for i, vid in enumerate(video_list):
            self.video_widgets.append(gui.Label(350, 12, video_list[i]))
            self.video_widgets.append(gui.VideoPlayer(480, 370, video_list[i]+'.mp4',
                    'http://www.oneparallel.com/wp-content/uploads/2011/01/placeholder.jpg'))
            #self.video_widgets[2*i+1].attributes['autoplay'] = 'autoplay'
            self.wid.append('placeholder_'+str(2*i), self.video_widgets[2*i])
            self.wid.append('placeholder_'+str(2*i+1), self.video_widgets[2*i+1])

    def _(self, txt):
        if not self.translations:
            with open("translations.yaml", 'r') as stream:
                self.translations = yaml.load(stream)
                pprint("translations")
                pprint(self.translations)
        return self.translations.get(txt, txt)

if __name__ == "__main__":
       start(MyApp,
          address='0.0.0.0',
          port=8081,
          multiple_instance=False,
          enable_file_cache=True,
          update_interval=0.1,
          start_browser=True)

