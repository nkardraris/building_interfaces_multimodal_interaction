__author__ = 'antman'
import os
import signal
import subprocess
import sys
import time
import warnings


def start(file, dir, topics):
    if not "roscore" in subprocess.check_output(["ps", "-ae"]):
        warnings.warn('error: roscore is not running')
        return None
    if not is_tool("rosbag"):
        warnings.warn('error: rosbag not on the system path make sure environment is initialized')
        return None
    args = ""
    if file:
        args = args + " " + "--output-name=" + file
    for topic in topics:
        args = args + " " + topic
    command = "rosbag record" + args
    return subprocess.Popen(command, stdin=subprocess.PIPE, shell=True, cwd=dir, preexec_fn=os.setsid)
	
def stop(p):
    if not hasattr(p,'pid'):
        warnings.warn("No process to finish")
        return
    os.killpg(p.pid, signal.SIGINT)

def is_tool(name):
    # https://stackoverflow.com/a/11210902
    try:
        devnull = open(os.devnull,"w")
        subprocess.Popen([name], stdout=devnull, stderr=devnull).communicate()
    except OSError as e:
        if e.errno == os.errno.ENOENT:
            return False
    return True

def example():
    t1 = time.time()
    print t1
    p = start("/home/mobot/test.rosbag", ".", ["/rosout"])
    print("pid: %d" % (p.pid))
    time.sleep(5)    
    stop(p)

if __name__ == "__main__":
    example()
