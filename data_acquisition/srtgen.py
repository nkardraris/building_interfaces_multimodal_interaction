#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'antman'

import time
import rospy
import warnings
from math import floor

class SrtGen:
    """
    srt generator.
    """
    def __init__(self):
        #rospy.init_node("AnnotatorNode", anonymous=True, disable_signals=True)
        self.filename = None
        self.id = 0
        self.state = {'recording': False, 'annotating': False}

    def t(self):
        #return rospy.get_rostime().to_nsec
        return time.time()*1000

    def start_recording(self, filename):
        if self.state['recording'] == True:
            warnings.warn("Attempt to start recording while recording")
            #return
        self.filename = filename
        #self.t0 = self.t()
        self.f = open(self.filename ,'w', 0)
        self.state['recording'] = True

    def stop_recording(self):
        if self.state['recording'] == False:
           warnings.warn("Attempt to stop recording while stopped")
           return
        self.f.close()
        self.state['recording'] = False
        self.id = 0

    def annotate(self,annotation):
        if self.state['recording'] == False:
           warnings.warn("Attempt to annotate while not recording")
           return
        if self.state['annotating'] == True:
           warnings.warn("Attempt to annotate while already annotating")
           return
        self.t1 = self.t()
        #self.t1 = self.t() - self.t0
        self.annotation = annotation
        self.state['annotating'] = True

    def end_annotation(self):
        if self.state['annotating'] == False:
           warnings.warn("Attempt to end annotation before starting")
           return
        t2 = self.t()
        #t2 = self.t() - self.t0
        self.output_string(t2)
        self.t1 = None
        self.state['annotating'] = False

    def output_string(self, t2):
        self.id += 1
        #_ = self.time_format
        #self.f.write(str(self.id) + '\n')
        self.f.write("%.10f" % self.t1 + ' ' + "%.10f" % t2 + ' ' + self.annotation + '\n')
        #self.f.write(self.annotation + '\n\n')

    def wrong_annotation(self):
        if self.state['annotating'] == False:
            warnings.warn("Attempt to delete annotation before annotating")
            return
        self.t1 = None
        self.state['annotating'] = False

    def time_format(self, milliseconds):
        seconds = floor(milliseconds / 1000)
        minutes = floor(seconds / 60)
        hours = floor(minutes / 60)
        milliseconds = milliseconds % 1000
        seconds = seconds % 60
        minutes = minutes % 60
        hours, minutes, seconds, milliseconds = map(int, (hours, minutes, seconds, milliseconds))
        srt_time = '{:d}:{:0>2d}:{:0>2d},{:0>3d}'.format(hours, minutes, seconds, milliseconds)
        return srt_time #rtrim(time, '0')

def test():
    srt = SrtGen()
    srt.start_recording("test.srt")
    time.sleep(1)
    srt.annotate("annotation A - 1 sec passed, 1 sec duration")
    time.sleep(1)
    srt.end_annotation()
    srt.annotate("to be canceled - 2 sec passed")
    time.sleep(1)
    srt.wrong_annotation()
    srt.annotate("annotation B - 3 sec passed, 1 sec duration")
    time.sleep(1)
    srt.end_annotation()

    print srt.time_format(1448367245.92)
    t = 3600*1000
    print srt.time_format(t)
    t = 100*t + t/60 + t/3600 + t/3600/1000
    print srt.time_format(t)

    #time gives seconds, want millisseconds
    t1= time.time()*1000
    time.sleep(2)
    t2 = time.time()*1000
    print t2-t1
    print srt.time_format(t2 - t1)


    print "TEST WARNINGS"
    srt.end_annotation()
    srt.start_recording("test.srt")
    srt.wrong_annotation()

    srt.stop_recording() # That is correct...
    srt.stop_recording()


if __name__ == "__main__":
    test()

