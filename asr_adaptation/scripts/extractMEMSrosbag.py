#!/usr/bin/env python
import os
import rosbag
import sys
from std_msgs.msg import Int32, String
import matlab_wrapper as mw

thebag=sys.argv[1]
outfile=sys.argv[2]

audio_topic='audiorecord'

d = os.path.dirname(sys.argv[0]) 

#ml = mw.MatlabSession(matlab_root = '/home/mobot/matlab')
ml = mw.MatlabSession(matlab_root = '/usr/local/MATLAB/R2013a/')
          
bag = rosbag.Bag(thebag ,'r')
print "opening rosbag file..."
f = open(outfile + '.raw','wb')
print "writing meassages to file..."
timestampsnsec = []
for topic,msg,t in bag.read_messages(topics = [audio_topic]):
    f.write(msg.data)
    timestampsnsec.append(t.to_nsec()/1000000000)

bag.close()
ml.put('outfile', outfile)
ml.put('d', d)
print "converting raw to wav..."
ml.eval('addpath(d)')
ml.eval('writemems(outfile)')
print "DONE"

timestampsnsec.sort(key=type(timestampsnsec[0]))
print 'First time stamp: %f\n' % (timestampsnsec[0])
