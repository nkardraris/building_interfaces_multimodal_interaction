models_dir=$1
scp=$2
mlf=$3
dict=$4
hvite_cfg=$5
herest_cfg=$6
adaptation_dir=$7
global_class=$8

# adaptation variables/parameters
nrc=32

# regression tree
rm -r $adaptation_dir/classes_$nrc
mkdir -p $adaptation_dir/classes_$nrc
cp $global_class $adaptation_dir/classes_$nrc/global
echo LS \"$models_dir/stats\" > $adaptation_dir/regtree.hed
echo RC $nrc \"rtree\" >> $adaptation_dir/regtree.hed
HHEd -T 1 -B -H $models_dir/models -M $adaptation_dir/classes_$nrc \
	$adaptation_dir/regtree.hed $models_dir/hmmlist

# alignment
HVite -A -T 1 -l '*' -b SENT-START -a -t 250.0 150.0 2000.0 -l $adaptation_dir/ \
    -H $models_dir/models -m -o SWT -C $hvite_cfg -S $scp \
    -X lab -y lab -I $mlf $dict $models_dir/hmmlist

# adaptation
mkdir $adaptation_dir/xforms
HERest -A -T 1 -C $herest_cfg -S $scp\
   -H $models_dir/models -u a -L $adaptation_dir -K $adaptation_dir/xforms mllr1 \
   -J $adaptation_dir/classes_$nrc -h '*.%%%' $models_dir/hmmlist
