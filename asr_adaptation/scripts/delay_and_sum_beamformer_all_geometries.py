import numpy as np
#import scipy.io.wavfile
import wave,struct


def next_greater_power_of_2(x):  
	return 2**(x-1).bit_length()


def time_delay_of_arrival(sensor_positions, source_position, c, F_s):
	num_sensors = sensor_positions.shape[1]
	r_source = np.sqrt(np.sum(source_position**2,0))
	r_sensors_source=np.zeros((num_sensors,1))
	for sensor_index in range(num_sensors-1,-1,-1):
		r_sensors_source[sensor_index,0]=np.sqrt(np.sum((sensor_positions[:,sensor_index]-source_position)**2))
	T=(r_sensors_source-r_source) / c * F_s
	a=r_source / r_sensors_source
	return T,a



def degreesTOradians(angle_deg):
	rad_per_deg = np.pi / 180
	angle_rad = rad_per_deg * angle_deg
	return angle_rad

def dft_radian_frequencies(N_fft):
	k_upper = np.floor((N_fft - 1) / 2)
	k_lower = 1 + k_upper - N_fft
	omega =  np.arange(k_lower,k_upper+1).transpose() / N_fft * (2 * np.pi)
	omega = np.fft.ifftshift(omega)
	return omega


def DSB_weights_linear_arrays(sensor_positions, source_position, c, F_s, N_fft):
	T,a = time_delay_of_arrival(sensor_positions, source_position, c, F_s)
	T_samples = T
	N = sensor_positions.shape[1]
	# DFT radian frequencies in [-pi, pi)
	omega = dft_radian_frequencies(N_fft)
	W=np.zeros((N,N_fft))+1j*np.zeros((N,N_fft))
	# DS beamformer weights are phase shifts (equivalent to temporal shifts in
	#                   the time domain) that compensate for propagation delays
	for sensor_index in range(0,N):
    		W[sensor_index, :] = a[sensor_index] * np.exp(- 1j * omega * T_samples[sensor_index])
	return W


def delay_and_sum_beamf(X, sensor_positions, source_position, c, F_s, N_fft):
	# Calculate DS beamformer weigths
	W = DSB_weights_linear_arrays(sensor_positions, source_position, c, F_s, N_fft)

	# DFT of input signals
	X_dft = np.fft.fft(X, N_fft, 0)
	Y_dft= np.zeros((N_fft,1))+1j*np.zeros((N_fft,1))
	# Apply DS beamformer weights and sum
	Y_dft = (np.sum(W.conj().transpose() * X_dft, 1))

	# DS beamformer output in the time domain
	y_zero_padded = np.fft.ifft(Y_dft, axis= 0)

	# Discard imaginary parts occuring due to finite numerical precision
    	y_zero_padded = np.real(y_zero_padded)


	# Truncate DS beamformer output to original signal length
	signal_length = X.shape[0]
	ytemp = np.zeros((signal_length,1))
	if (N_fft > signal_length):
		ytemp = y_zero_padded[0:signal_length]
   		y = ytemp.copy()
	else:
    		y = y_zero_padded.copy()
	
	# Normalize amplitude
	y = np.divide(y,X.shape[1])
	
	return y

def read_wav(wavfile):	
	
	# read wave info
	wave_obj = wave.open(wavfile,'r')
	fs = wave_obj.getframerate()
	wave_len = wave_obj.getnframes()
	nbytes = wave_obj.getsampwidth()

	# read wave binary data and convert to signed shorts
	wave_bin_data = wave_obj.readframes(wave_len)
	wave_data = struct.unpack('@{0}h'.format(wave_len), wave_bin_data)
	
	# store to numpy array
	s = np.array(wave_data)

	return fs,s,nbytes

def write_wav(s,fs,nbytes,wavfile):
	# open wave for writing
	wave_obj = wave.open(wavfile,'w')
	wave_obj.setframerate(fs)
	wave_obj.setnchannels(1)
	wave_obj.setsampwidth(nbytes)
	
	# pack to signed shorts
	packed = struct.pack('@{0}h'.format(len(s)),*s)

	# write and close
	wave_obj.writeframes(packed)
	wave_obj.close()
	
