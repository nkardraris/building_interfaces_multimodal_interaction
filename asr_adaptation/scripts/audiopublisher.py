#!/usr/bin/env python
import roslib

import time
import getopt
import alsaaudio
import sys
import rospy
from std_msgs.msg import String
 
def audiopublisher():
	pub = rospy.Publisher('audiorecord', String)
	
	rospy.init_node('audiopublisher')
	r = rospy.Rate(48000) 
	
    	
    		

   	card = "hw:CARD=Mod"

  
   	

  	# Open the device in nonblocking capture mode. The last argument could
  	# just as well have been zero for blocking mode. Then we could have
   	# left out the sleep call in the bottom of the loop
   	inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NONBLOCK, card)

   	# Set attributes: 8 channels, 48000 Hz, 16 bit little endian samples
   	inp.setchannels(8)
   	inp.setrate(48000)
   	inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)

   	# The period size controls the internal number of frames per period.
   	# The significance of this parameter is documented in the ALSA api.
   	# For our purposes, it is suficcient to know that reads from the device
   	# will return this many frames. Each frame being 2 bytes long.
  	# This means that the reads below will return either 320 bytes of data
   	# or 0 bytes of data. The latter is possible because we are in nonblocking
   	# mode.
   	inp.setperiodsize(160)
    	while not rospy.is_shutdown():
   		
   		
       			
       			
       		l, data = inp.read()
      		
       		if l:
			#print(l)
			pub.publish(String(data))
			
           		time.sleep(.001)

       		
		r.sleep()
if __name__ == '__main__':
     try:
       	audiopublisher()
     except rospy.ROSInterruptException:
        pass




