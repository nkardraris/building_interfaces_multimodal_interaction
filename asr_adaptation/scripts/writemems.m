function writemems(filename)
X = readraw(filename, 7, 'int16', 'l');
for i=1:7
	wavfile=strcat(filename,'_mems_ch',num2str(i),'.wav');
	wavwrite(X(:,i)/max(X(:,i)),48000,wavfile);
end
