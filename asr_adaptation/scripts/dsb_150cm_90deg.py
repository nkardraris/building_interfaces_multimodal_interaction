import numpy as np
#import scipy.io.wavfile
import delay_and_sum_beamformer_all_geometries as dsb
#import scipy.io
import sys,glob


wav_pat=sys.argv[1]

# MOBOT Task6a setup: Source and microphone coordinates in reference with the center of the linear array
ch_names= ['ch1','ch2','ch3','ch4','ch5','ch6','ch7']
N = len(ch_names)
source = np.zeros((3,1))
source = [0,150,150]
source = np.array(source)
sensor = np.zeros((3,N))
sensor = [[-30,-20,-10,0,10,20,30],[0,0,0,0,0,0,0],[100,100,100,100,100,100,100]]
sensor = np.array(sensor)

# allocate space to read the channels
#Fs,Xfirst = scipy.io.wavfile.read('./athenadb_test_sess/signals/OA6.wav')
fs,tmp,nbits = dsb.read_wav(wav_pat.replace("*","ch1"))
siglen=tmp.shape[0]
xc = np.zeros((siglen,N))

# read MEMS channels
for i in range (0,N):
	#Fs,xoldy = scipy.io.wavfile.read('./athenadb_test_sess/signals/'+listaki[i]+'.wav')
	#xoldy = np.array(xoldy)
	fs,tmp,nbytes = dsb.read_wav(wav_pat.replace("*",ch_names[i]))
	xc[:,i] = tmp 

xc=np.array(xc)

# beamforming of segments
ds=xc[:,4].copy()

winlen=int(np.ceil(60 * fs))
for i in range(0,siglen,winlen):
  tmp=xc[i:min(i+winlen-1,siglen)][:].copy()
  l=tmp.shape[0]
  N_fft = dsb.next_greater_power_of_2(l)
  s = dsb.delay_and_sum_beamf(tmp,sensor, source, 34000, fs,N_fft)
  ds[i:min(i+winlen-1,siglen)]=s

ds=ds/1.5
# write beamformed signal 
dsb.write_wav(ds,fs,nbytes,wav_pat.replace("*","dsb"))

