### Training for the online recognition system

The training system (which is used to train for the online system, as well as perform offline testing), is located in gesture\_training/. The actual code is in gesture\_training/Code. To use the code and train visual action/gesture models follow the steps below:  
- Compile external libraries provided under gesture\_training/Code/external. There are three libraries used (dense trajectories, yael, libsvm) and each one comes with its own README file which you should follow. For libsvm and yael make sure you compile the matlab interface as well.  
- Training is parametrized by a configuration file; a sample file is provided in gesture\_training/configs/sample\_config.yaml". Please check the sample file, where all parameters are explained, and write your own configuration file that matches your local setup.  
- Training is carried out by running the following commands in matlab:  
\>> cd 'gesture\_training/'  
\>> runOnlineExperiment('gesture\_training/configs/sample\_config.yaml')  

### Using the trained models for online recognition
Training is carried out with the leave-one-out scheme, i.e. testing on one sub-ject, while the rest of the subjects are used for training; this is repeated for all subjects. Using the naming convention in the configuration file (please check gesture\_training/configs/sample\_config.yaml), the generated data from the training stage are stored in root_path/dbName/basename/training/test\_subjectID/converted\_c and models_libsvm_format/desc. The files needed for the online system are:  
- The codebook: codebook\_desc  
- The BoW histohrams of the training data : encoded_features\_desc  
- The kernel normalisation factor: norm\_desc  
- SVM models (their number equals the number of gestures in your vocabulary plus one if you use a background model)  

In the above "desc" stands for the descriptor you want to use (Trajectory, HOG, HOF or MBH).