import sys
import cv2
import os
import code
import rosbag
from cv_bridge import CvBridge, CvBridgeError
import argparse
import yaml
from math import ceil, sqrt, pi, exp


def main(args):
    parser = argparse.ArgumentParser(
        description="Visualize kinect RGB frames existing in bagfiles and (optionaly) save them as videos.")
    parser.add_argument('bagfile', metavar='ros_bagfile.bag', help='a ROS bagfile')
    parser.add_argument('annotation_file', metavar='text_file.txt', help='an annotation file')
    parser.add_argument('--decimation', '-d', default=1, type=int, choices=[1, 2, 4, 8, 16], help='decimation factor')
    parser.add_argument('--stride', '-s', default=1, type=int,
                        help='frame stride: e.g. if frame stride=3, frames 0, 3, 6, ... are kept')
    parser.add_argument('--topic', '-t', default="/camera/rgb/image_color",
                        help='the ROS topic to read from')
    parser.add_argument('--output', '-o', default=False, type=str,
                        help='video file where kinect frames will be saved')
    parser.add_argument('--visualize', '-v', default=0, type=int, choices=[0, 1],
                        help='show video')

    # Parse arguments
    args = parser.parse_args()
    bagfile = args.bagfile
    annotation_file = args.annotation_file
    topic = args.topic
    output = args.output
    visualize = args.visualize
    decim_f = args.decimation
    stride = args.stride
    # print bagfile, annotation_file, topic, output, visualize

    # Configure output video and annotation file (if applies)
    (rosbag_path, rosbag_fileName) = os.path.split(bagfile)
    (rosbag_fileBaseName, rosbag_ext) = os.path.splitext(rosbag_fileName)
    if not (rosbag_ext == ".bag"):
        print("%s is not a valid rosbag\n" % rosbag_fileName)
        sys.exit()
    if output:
        if not os.path.isdir(output):
            os.mkdir(output)
        output_path = os.path.join(output, rosbag_fileBaseName)
    else:
        output_path = os.path.join(rosbag_path, rosbag_fileBaseName)
    if not os.path.isdir(output_path):
        os.mkdir(output_path)
    output_annotations = os.path.join(output_path, rosbag_fileBaseName + '.txt')
    output_video = os.path.join(output_path, rosbag_fileBaseName + '.avi')

    print(output_path, output_annotations, output_video)

    # code.interact(local=locals())

    # read bagfile
    bag = rosbag.Bag(bagfile, 'r')  # open rosbag
    # Check if topic exists
    info_dict = yaml.load(bag._get_yaml_info())
    if ('topics' not in info_dict):
        print("No topics in '" + bagfile + "'. Check your rosbag.")
        sys.exit()
    else:
        topics = [info_dict['topics'][i]['topic'] for i in xrange(len(info_dict['topics']))]
        if topic not in topics:
            print("No topic '" + topic + "' in rosbag '" + bagfile + "'. Available topics in this file are:")
            print(topics)
            sys.exit()
    # Get all available timestamps
    timestamps = [msg[1].header.stamp.to_sec() * 1000 for msg in bag.read_messages(topics=topic)]

    # map annotations timestamps --> frame numbers
    with open(annotation_file, "r") as anot_file:
        annotations = [line.strip('\n') for line in open(annotation_file, "r").readlines()]
        annotations = [line.split(" ") for line in annotations if line]
        start_times = [float(annotations[i][0]) for i in xrange(len(annotations))]
        end_times = [float(annotations[i][1]) for i in xrange(len(annotations))]
        labels = [annotations[i][2] for i in xrange(len(annotations))]
        start_frames = [min(enumerate(timestamps), key=lambda x: abs(x[1] - t))[0] for t in start_times]
        end_frames = [min(enumerate(timestamps), key=lambda x: abs(x[1] - t))[0] for t in end_times]

    # Write annotations
    with open(output_annotations, "w") as a:
        for l in xrange(len(labels)):
            a.write("%d %d %s\n" % (start_frames[l], end_frames[l], labels[l]))


    if visualize:
        cv2.namedWindow("Kinect - RGB", 1)

    # Start Processing the bagfile
    msg_gen = bag.read_messages(topics=topic)  # reset generator on each loop
    bridge = CvBridge()
    image_list = []  # initialize frame buffer
    actual_frame_counter = 0
    frame_counter = 0
    while (True):
        try:  # get next frame
            topic, msg, t = msg_gen.next()
        except StopIteration:  # all frames have been catched
            break
        #print frame_counter, stride, frame_counter % stride
        actual_frame_counter += 1
        if (stride>1) and ((actual_frame_counter-1) % stride != 0):
            continue

        cv_image = bridge.imgmsg_to_cv2(msg, desired_encoding="bgr8")
        output_file = os.path.join(output_path, "image%06d.png" % frame_counter)

        # Decimation
        if decim_f > 1:
            size = (int(ceil(cv_image.shape[1] / 2)), int(ceil(cv_image.shape[0] / 2)))
            cv_image = cv2.pyrDown(cv_image, dstsize=size)
            if decim_f > 2:
                size = (int(ceil(cv_image.shape[1] / 2)), int(ceil(cv_image.shape[0] / 2)))
                cv_image = cv2.pyrDown(cv_image, dstsize=size)
                if decim_f > 4:
                    size = (int(ceil(cv_image.shape[1] / 2)), int(ceil(cv_image.shape[0] / 2)))
                    cv_image = cv2.pyrDown(cv_image, dstsize=size)
                    if decim_f > 8:
                        size = (int(ceil(cv_image.shape[1] / 2)), int(ceil(cv_image.shape[0] / 2)))
                        cv_image = cv2.pyrDown(cv_image, dstsize=size)

        cv2.imwrite(output_file, cv_image)
        #if frame_counter == 0:
        #    height, width, channels = cv_image.shape
        #    video_writer = cv2.VideoWriter()
        #    retval = video_writer.open(output_video, cv2.cv.CV_FOURCC('M', 'J', 'P', 'G'), 25, (height, width))
        #    print(height, width, retval)

        #video_writer.write(cv_image)
        frame_counter += 1
        is_annotated = [actual_frame_counter > start_frames[i] and actual_frame_counter < end_frames[i] for i in
                        xrange(len(start_frames))]
        # print frame_counter, any(is_annotated)

        # code.interact(local=locals())
        if visualize:
            if any(is_annotated):
                ind = [i for i in xrange(len(is_annotated)) if is_annotated[i]][0]
                cv2.putText(cv_image, str(ind + 1) + '. ' + labels[ind], (20, 450), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (255, 50, 50), 2, cv2.CV_AA)
            cv2.imshow("Kinect - RGB", cv_image)
            key = cv2.waitKey(25)
            if key == 1048689 or key == 1179729:
                break

    #video_writer.release()
    if visualize:
        cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv)
