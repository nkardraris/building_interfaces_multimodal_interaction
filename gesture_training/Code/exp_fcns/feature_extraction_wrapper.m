function f = feature_extraction_wrapper(params, db_params)

for c=1:length(db_params.clips)
    clip = db_params.clips(c);
    if (params.reuse.features) && exist(fullfile(params.paths.features, sprintf('%s.mat', clip.name)), 'file')
        try
            f = parload(fullfile(params.paths.features, clip.name));
        catch
            params.reuse.features = false;
            f = feature_extraction_wrapper(clip, params);
        end
    else
        f = extractFeatures(clip, params.features);
        parsave(fullfile(params.paths.features, clip.name), f);
    end
    
    d = params.features.descriptors;
    if ~isempty(find(isnan(f.(d{1})), 1))
        params.reuse.features = false;
        f = feature_extraction_wrapper(clip, params);
    end
end
end