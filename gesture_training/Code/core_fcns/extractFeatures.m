function features = extractFeatures(clip, params, varargin)
switch params.type
    case 'DT'
        features = extractDT(clip, params);        
    otherwise
        error('Unrecognized feature: %s\n', params.type);
end
end

function features = extractDT(clip, params)

%% Set environmental variable
if isfield(params, 'libs') && exist(params.libs, 'file')
    ffmpeglibsPath = params.libs;
else
    ffmpeglibsPath = '/usr/local/lib/';
end

ldPath = getenv('LD_LIBRARY_PATH');
setenv('LD_LIBRARY_PATH',ffmpeglibsPath);

%% Extract features

% note: segments are annotated starting from frame 1, DenseTrack
% counts frames starting from 0
command = sprintf('%s %s -S %d -E %d -L %d -W %d -N %d -s %d -t %d ',...
    params.executable, clip.frames, clip.startFrame-1, clip.endFrame -1,...
    params.L, params.W, params.N, params.s, params.t);
% file_size = extractfield(dir(clip.file), 'bytes')/(1024*1024); % file size in MB
[~, file_name] = fileparts(clip.rosbag);
temp_file = fullfile(params.path, sprintf('temp_%s', file_name));
command = sprintf('%s > %s', command, temp_file);
status = system(command);
cmdout = fopen(temp_file);
if status
    error('Error extracting features');
end

%% Parse features
format = repmat('%f\t', 1, 10+2*params.L+3*96+108); format = format(1:end-2);
f = textscan(cmdout, format);
if ~isempty(f)
    f = horzcat(f{:});
    features.info = f(:,1:10);
    for ii=1:length(params.descriptors)
        switch params.descriptors{ii}
            case 'Traj'
                features.Traj = f(:,11:10+2*params.L);
            case 'HOG'
                features.HOG = f(:,10+2*params.L+1:10+2*params.L+96);
            case 'HOF'
                features.HOF = f(:,10+2*params.L+96+1:10+2*params.L+96+108);
            case 'MBHx'
                features.MBHx = f(:,10+2*params.L+96+108+1:10+2*params.L+2*96+108);
            case 'MBHy'
                features.MBHy = f(:,10+2*params.L+2*96+108+1:10+2*params.L+3*96+108);
            case 'MBH'
                features.MBH = f(:,10+2*params.L+96+108+1:10+2*params.L+3*96+108);
            otherwise
                error('Unrecognized descriptor: %s\n', params.descriptors{ii})
        end
    end
end

fclose(cmdout);
delete(temp_file);

end
