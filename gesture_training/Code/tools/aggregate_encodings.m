function [enc, empty] = aggregate_encodings(input_path, output_path, clips)
i = 1;
while true
    f0 = parload(fullfile(input_path, sprintf('%s.mat', clips(i).name)));
    if ~isempty(f0)
        break
    end
    i = i+1;
end
d = fieldnames(f0);
empty = [];
enc = init_struct_array(d,length(clips));
for i=1:length(clips)
    e = parload(fullfile(input_path, sprintf('%s.mat', clips(i).name)));
    if isempty(e)
        empty = [empty i];
    else
        enc(i) = e;
    end
end
enc(empty) = [];
for id = 1:length(d)
    encoded_features = cell2mat({enc.(d{id})}');
    parsave(fullfile(output_path, sprintf('encoded_features_%s.mat', d{id})), encoded_features);
end

