function mat2c(input_directory, output_directory)

if ~exist('output_directory', 'var')
    output_directory = fullfile(input_directory, 'converted_c');
end

if ~exist(output_directory, 'dir')
    mkdir(output_directory);
end

mat_files = dir(fullfile(input_directory, '*.mat'));
mat_filenames = {mat_files.name}';
[~, mat_basenames] = cellfun(@fileparts, mat_filenames, 'UniformOutput', false);

for f=1:length(mat_files)    
    if (~isempty(strfind(mat_basenames{f}, 'model')))
        continue;
    end
    
    all_vars = load(fullfile(input_directory, mat_filenames{f}));
    var_names = fieldnames(all_vars);
    if length(var_names) > 1 
        fprintf('File %s contains more than 1 variables. Skipping.\n', mat_files(f).name)
        continue;
    end
    var = all_vars.(var_names{1});
    
    output_file = fullfile(output_directory, mat_basenames{f});
    [ms, is] = max(size(var));
    if (~isempty(strfind(mat_basenames{f}, 'encoded_features')) && (is==1) ) || ...
            (~isempty(strfind(mat_basenames{f}, 'codebook')) && (is==2) )
            var=var';
    end
    if ms==1
        fid = fopen(output_file, 'w');
        fprintf(fid, '%.20f\n', var);
        fclose(fid);
    else
        dlmwrite(output_file, var, 'delimiter', ' ', 'precision','%.20f');
    end
end
    
    
    
    
end
    