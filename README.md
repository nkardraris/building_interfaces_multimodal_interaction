### A Complete Framework for Multimodal Interaction
This repository contains an on-line audio-gestural command recognition system that has been developed in the context of [MOBOT project](http://www.mobot-project.eu/), as well as a complete framework that can be used to build new human-computer interfaces that rely on audio and gesture commands. You can also refer to the [project page](http://robotics.ntua.gr/software/building-interfaces-multimodal-interaction/) and our ACM OSSC16 submission paper.  

There are 4 individual components in this package, namely:  
1. an on-line demo of our multimodal gesture recognition system with pre-trained models (online_multimodal/)  
2. training code for visual recognition of gestures (gesture_training/)  
3. speech adaptation code for the spoken command recognition system (asr_adaptation/)  
4. a data acquisition tool to assist the collection and annotation of videos that can be used by (2) and (3) (data_acquisition/).  
There is a README file inside each individual component's directory.  

Note that (1) and (2) depend on the Robot Operating System (ROS). Our online recognition system has been tested and can be used with any commercial camera and microphone that is compatible with ROS. Nevertheless, "Kinect for Windows" and an 8-channel MEMS microphone array have been used to train our pre-trained models.

